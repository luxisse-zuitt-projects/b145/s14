//displays the message
console.log("Hello from JS");

// this is a single-line comment

/* multi-line comment
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
*/

// ctrl + /
console.log("example statement");

// syntax: console.log(value/message);

//[DECLARING VARIABLES]
// --> tell our devices that a variable name is created and ready to store data.

// syntax: let/const desiredVariableName;

let myVariable;

// WHAT is a variable?
	// ==> It is used to contain/store data.

// Benefits of utilising a variable?
	// This makes it easier for us to associate information stored in our devices to actual names about the information.

// an assignment operator (=) is used to assign/pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNumber = "09951446335";

//[PEEKING INSIDE A VARIABLE]
let greetings;

console.log(clientName);
console.log(contactNumber);
console.log(greetings);
let pangalan = "John Doe"; 
console.log(pangalan);



// If we would try to print out a value of a variable that has not been declared a value, it will return a state of "undefined."

// invocation before declaration

//[DATA TYPES]

// 3 Forms of Data Types:

// 1. Primitive -> contains only a single value:
// 		ex: strings, numbers, boolean
// 2. Composite -> can contain multiple values
// 		ex: array, objects
// 3. Special -> special characteristics
// 		ex: null , undefined


// 1. String - primitive data type: only contain a single value
let country = "Philippines";
let province = 'Metro Manila';
let fullAddress = province + ', ' + country;

// 2. Numbers - primitive data type: only contain a single value
let headcount = 26;
let grade = 98.7;
let planetDistance = 2e10;

// 3. Boolean - value that answers the question: true or false; primitive data type: only contain a single value
let isMarried = false;
let inGoodConduct = true;

// 4. Null - indicates the absence of a value; special data type
let spouse = null;
console.log(spouse);
let criminalRecords = true;
console.log(criminalRecords);

// 5. Undefined - represents the state of a variable that has been declared but without an assigned value; special data type

let fullName;

// 6. Arrays
// Arrays are a special kind of composite data type that is used to store multiple values.

let grades = [98.7, 92.1, 90.2];

 //lets create a collection of all your subjects in the bootcamp
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];

//display the output of the array inside the console
console.log(bootcampSubjects);

// Rule of Thumb when declaring array structures
// ==> Storing multiple data types inside an array is NOT recommended. In a context of programming, this does not make any sense.
// An array should be a collection of data that describes a similar/singe topic or subject.
let details = ["Keanu", "Reeves", 32, true];
console.log(details);

// 7. Object - contains other data or a set of information (array of personal information for different persons); composite data type

// Objects
	//Objects are another special kind of composite data type that is used to mimic or represent a real world object/item.
  	//They are used to create complex data that contains pieces of information that are relevant to each other.
  	//Every individual piece of code/information is called property of an object.

// SYNTAX:
	// 	let/const objectName = {
	// 		key -> value
	// 		propertyA: value,
	// 		propertyB: value
	// 	}

// let's create an object that describes the properties of a cellphone

let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000
}

// let's display the object inside the console.
console.log(cellphone);

// Variables and Constants

// Variables are used to store data.

// the values/info stored inside a variable can be changed or repackaged.
let personName = "Michael";
console.log(personName);

// If you're going to reassign a new value for a variable, you no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

// concatenating string (+)
	// ==> join, combine, link
let pet = "dog";
console.log("this is the initial value of var: " + pet);

pet = "cat";
console.log("This is the new value of the var: " + pet);

// Constants
	// => Permanent, fixed, absolute
	// => the value assigned on a constant cannot be changed.
	// syntax: const desiredName = value;
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

// let's try to reassign a new value in a constant.
familyName = 'Castillo';
console.log(familyName);